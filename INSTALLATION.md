Клонировать репозиторий:

```bash
git clone git@bitbucket.org:zippbl4/october-cms-skeleton.git
```

Создать и заполнить `.env`:

```bash
php -r "file_exists('.env') || copy('.env.example', '.env');"
```

или:

```bash
composer run-script post-root-package-install
```

Сгенерировать секретный ключ приложения:

```bash
php artisan key:generate --ansi
```

Обязательно заполнить следующие поля в `.env`:

```
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

Выполнить установку:

```bash
composer install
```

Создать новую тему и сделать ее активной (правка `.env`):

```
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

